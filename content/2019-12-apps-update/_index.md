---
title: KDE's December 2019 Apps Update
publishDate: 2019-12-12 00:01:00
layout: page # don't translate
summary: "What Happened in KDE's Applications This Month"
type: announcement # don't translate
---

NOT PUBLISHED YET

<!--
git diff 'HEAD@{1 month ago}' HEAD

ssh ftpadmin@master.kde.org 'find stable/ -mtime 31'
-->

## Calligra Plan Reloaded

A big release this month is that of [Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan). A huge milestone, since the prior version of the project planning and management tool was released almost two years ago.

{{< figure class="text-center" src="https://dot.kde.org/sites/dot.kde.org/files/kplan-wee.png" link="https://dot.kde.org/sites/dot.kde.org/files/kplan.png" caption="Calligra Plan" >}}

Plan is intended for managing moderately large projects with multiple resources. To enable you to model your project adequately, Plan offers different types of task dependencies and timing constraints. Usually you will define your tasks, estimate the effort needed to perform each task, allocate resources and then schedule the project according to the dependency network and resource availability.

A Gantt chart is a horizontal bar chart developed as a production control tool in 1917 by Henry L. Gantt, an American engineer and social scientist. Frequently used in project management, a Gantt chart provides a graphical illustration of a schedule that helps to plan, coordinate, and track specific tasks in a project. KPlan has great support for these charts which help you view your project's workflow.

## Digikam 6.4

## Tellico 3.2.2

## Krita 6.2.8

On November 27, 2010, the Krita team released an bug fix release. This release fix problems with saving Krita file on windows and vector shapes.

{{<learn-more href="https://krita.org/en/item/krita-4-2-8-released/" >}}

## Kdenlive

this version comes with more that 200 commits

* Fix huge memory consumption and inefficiency of audio thumbnails causing freeze on high zoom
* Use QVector instead of QList to store audio thumbnails

New audio mixer: New feature!

Performance and usability improvements

Many Windows fixes

New audio clip display in clip monitor and project bin

## Dolphin

* Easily see which programs are preventing a device from being unmounted <https://bugs.kde.org/show_bug.cgi?id=189302>
* Redesigned advanced search options for easier navigation <https://phabricator.kde.org/D24602>
* Go back and forward multiple times by long-pressing the arrows <https://bugs.kde.org/show_bug.cgi?id=157819>
* Recently saved/accessed has been reworked and actually works now <https://phabricator.kde.org/D7446>
* Preview GIF's by hovering over the preview pane <https://bugs.kde.org/show_bug.cgi?id=182257>
* Reset zoom levels <https://bugs.kde.org/show_bug.cgi?id=409591>
* Click-to-play for videos and audios in the preview pane <https://phabricator.kde.org/D22183>
* Thumbnails for .cb7 comic books <https://bugs.kde.org/show_bug.cgi?id=410696>

## Gwenview

* Adjust JPEG compression level on saving <https://bugs.kde.org/show_bug.cgi?id=277996>
* Performance improvements for remote images

## Konsole

* No particularily notable new features.
* Many bug fixes, e.g. less screen artifacts with HiDPI screens <https://phabricator.kde.org/R319:2c10032170ac9c01b71bd4d405c2d7ba40786c68>

## Okular

* Inertial scrolling massively improves the touch experience of Okular <https://bugs.kde.org/show_bug.cgi?id=413989>
* Annotation times are now correctly changed to the local time zone instead of always being GMT <https://bugs.kde.org/show_bug.cgi?id=413591>
* Support for the .cb7 comic book format has been added <https://phabricator.kde.org/D23037>

## Elisa

* the first release in 19.12

## Spectacle

* Add new touch-friendly drag handler [D23322](https://phabricator.kde.org/D23322)
* New autosave function, useful for rapidly taking multiple screenshots <https://phabricator.kde.org/D23210>
* Awesome animated progress button <https://phabricator.kde.org/D22324>

## Plasma Browser Integration 1.7

On November 26, 2019, Plasma Browser Integration 1.7 was released. This release features a blacklist for the media controls functionality, allow storing the origin URL for downloads and add support for the Web Share API.

{{<learn-more href="https://blog.broulik.de/2019/11/plasma-browser-integration-1-7/" >}}

## Microsoft Store

Kile is now availabel on the Microsoft Store.

{{<microsoft-store href="https://www.microsoft.com/store/apps/9pmbng78pfk3" >}}

## New in Incubator

SubtitleComposer

## New in KDE Review

plasma-nano and plasma-phone-components in kde review
KTimeTracker into extragear

## kpublictransport into extragear/libs
## kquickcharts into frameworks

