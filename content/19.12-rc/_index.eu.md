---
title: 19.12 RC Kaleratzea
publishData: 2019-11-29 00:01:00
layout: page # don't translate
summary: "Banakako 120 programatik gora gehi hamabinaka programatzaile liburutegi eta eginbide plugin argitaratu dira batera KDEren argitalpen zerbitzuren zati gisa."
type: announcement # don't translate
---

2019ko azaroak 29. Banakako 120 programatik gora gehi hamabinaka
programatzaile liburutegi eta eginbide plugin argitaratu dira batera KDEren
argitalpen zerbitzuaren zati gisa.

Gaurkoan guztiek lortu dituzte argitalpen hautagai diren sorburuak, hau da,
eginbide osatuak dituztela baino probak egitea behar dutela azken akatsak
konpontzeko.

Banaketa eta aplikazio biltegietako paketatzaileek beraien
argitaratu-aurreko kanalak eguneratu beharko lituzkete arazoak ikuskatzeko.

+ [19.12 argitalpen
  oharrak](https://community.kde.org/Releases/19.12_Release_Notes) tar
  artxiboen eta arazo ezagunen gaineko informazio bila.
+ [Paketek zama-jaisteko wiki
  orria](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
+ [19.12 RC sorburuaren informazio
  orria](https://kde.org/info/applications-19.11.90)

## Prentsako kontaktuak

Informazioa zabaltzeko bidal iezaguzu eposta bat:
[press@kde.org](mailto:press@kde.org).
